rust-async-std (1.13.0-4) unstable; urgency=medium

  * tighten patch 2003 to drop (not empty) avoided features
  * simplify patch 2002
  * declare rust-related build-dependencies unconditionally,
    i.e. drop broken nocheck annotations
  * add metadata about upstream project
  * update git-buildpackage config:
    + filter out debian subdir
    + simplify usage comments
  * update watch file:
    + improve filename mangling
    + use Github API

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 06 Feb 2025 13:02:42 +0100

rust-async-std (1.13.0-3) unstable; urgency=medium

  * fix patch 2004 to negate logic

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 16 Jan 2025 10:00:52 +0100

rust-async-std (1.13.0-2) unstable; urgency=medium

  * add patch 2004
    to silence fatal warning for documented unsafe example;
    closes: bug#1090294, thanks to Santiago Vila
  * update copyright info: update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 15 Jan 2025 02:11:24 +0100

rust-async-std (1.13.0-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Jonas Smedegaard ]
  * stop mention dh-cargo in long description
  * bump project versions in virtual packages and autopkgtests
  * provide virtual package and (commented out) autopkgtest
    for feature io_safety
  * update and unfuzz patches
  * tighten (build-)dependencies
    for crates async-lock async-process futures-lite
  * fix set buildsystem=rust in test rule

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 21 Sep 2024 16:41:59 +0200

rust-async-std (1.12.0-22) unstable; urgency=medium

  * autopkgtest-depend on dh-rust (not dh-cargo)
  * add patch 1001_futures-lite
    to accept newer branch of crate futures-lite;
    relax (build-)dependencies for crate futures-lite;
    closes: bug#1078652, thanks to Peter Green

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 14 Aug 2024 10:44:23 +0200

rust-async-std (1.12.0-21) unstable; urgency=medium

  * simplify packaging;
    build-depend on dh-sequence-rust
    (not dh-cargo libstring-shellquote-perl)

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 12 Jul 2024 12:28:52 +0200

rust-async-std (1.12.0-20) unstable; urgency=medium

  * fix patch 2004

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 04 Jul 2024 09:06:08 +0200

rust-async-std (1.12.0-19) unstable; urgency=medium

  * add patch 2004 to tolerate warnings in doctests;
    closes: bug#1074733, thanks to Lucas Nussbaum
  * relax (build-)dependencies
    for crates async-channel async-lock async-process

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 04 Jul 2024 03:04:32 +0200

rust-async-std (1.12.0-18) experimental; urgency=medium

  * update copyright info: fix file path
  * renumber/rename patches
  * add patches 1001_*
    to accept newer branches
    of crates async-channel async-lock async-process;
    tighten (build-)dependencies
    for crates async-channel async-lock async-process
  * relax to build- and autopkgtest-depend unversioned
    when version is satisfied in Debian stable
  * declare compliance with Debian Policy 4.7.0

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 25 Jun 2024 15:42:21 +0200

rust-async-std (1.12.0-17) unstable; urgency=medium

  * tighten (build-)dependency for crate async-io

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 11 Feb 2024 18:23:59 +0100

rust-async-std (1.12.0-16) unstable; urgency=medium

  * add patches 2001_async_io to accept older crate;
    relax (build-)dependencies for crate async-io
    (see bug#1061669)
  * update dh-cargo fork
  * update copyright info: update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 01 Feb 2024 21:20:44 +0100

rust-async-std (1.12.0-15) unstable; urgency=medium

  * revert to no longer avoid feature kv-log-macro:
    + drop patch 2004
    + (build-)depend on packages for crates kv-log-macro log
    + build-depend on package for crate femme
    (see bug#1040837)
  * update dh-cargo fork

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 31 Aug 2023 09:37:25 +0200

rust-async-std (1.12.0-14) unstable; urgency=medium

  * update dh-cargo fork;
    closes: bug#1047671, thanks to Lucas Nussbaum
  * update DEP-3 patch headers

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 14 Aug 2023 13:01:29 +0200

rust-async-std (1.12.0-13) unstable; urgency=medium

  * add patch 2004 to avoid feature kv-log-macro;
    stop (build-)depend on packages for crates kv-log-macro log;
    stop build-depend on package for crate femme;
    thanks to Fabian Grünbichler (see bug#1040837)

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 18 Jul 2023 12:25:38 +0200

rust-async-std (1.12.0-12) unstable; urgency=medium

  * tighten autopkgtests
  * add TODO note about enabling more autopkgtests

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 02 Feb 2023 17:48:17 +0100

rust-async-std (1.12.0-11) unstable; urgency=medium

  * change binary library package to be arch-independent
  * declare compliance with Debian Policy 4.6.2
  * stop superfluously provide virtual unversioned feature packages
  * update dh-cargo fork;
    build-depend on libstring-shellquote-perl
  * update copyright info: update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 01 Feb 2023 15:27:49 +0100

rust-async-std (1.12.0-10) unstable; urgency=medium

  * really enable only autopkgtest for feature default
    (not also --all-fetures)

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 10 Nov 2022 06:40:04 +0100

rust-async-std (1.12.0-9) unstable; urgency=medium

  * disable all autopkgtests except feature default:
    mysteriously fails where build-time test checking succeeds
    and seemingly code works fine in actual use

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 09 Nov 2022 22:22:49 +0100

rust-async-std (1.12.0-8) unstable; urgency=medium

  * revert to again provide features docs unstable;
    adjust autopkgtests to always include feature std

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 07 Nov 2022 08:16:40 +0100

rust-async-std (1.12.0-7) unstable; urgency=medium

  * revert to not bogusly set X-Cargo-Crate
  * add patch 2002
    to avoid dev-only feature surf and example surf-web
  * add patch 2003
    to avoid features tokio02 tokio03
  * add patch 2004
    to avoid feature unstable;
    stop (build-)depend on librust-async-process-1+default-dev;
    stop provide virtual packages
    librust-async-std+unstable-dev librust-async-std-1+unstable-dev
  * add patch 2005
    to avoid feature docs
  * update autopkgtests to avoid features unstable docs
  * check all targets and features during build

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 03 Nov 2022 22:25:37 +0100

rust-async-std (1.12.0-6) unstable; urgency=medium

  * fix have autopkgtests skip test io_timeout
  * fix set X-Cargo-Crate: async_std;
    closes: bug#1023031, thanks to Peter Green

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 31 Oct 2022 15:43:34 +0100

rust-async-std (1.12.0-5) unstable; urgency=medium

  * drop non-working patch 1001;
    always skip test io_timeout
  * avoid installing CI script

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 28 Oct 2022 12:55:47 +0200

rust-async-std (1.12.0-4) unstable; urgency=medium

  * add patch 1001
    to skip flaky io_timeout test on more archs

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 28 Oct 2022 11:28:52 +0200

rust-async-std (1.12.0-3) unstable; urgency=medium

  * re-release for building with auto-builder

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 28 Oct 2022 10:32:32 +0200

rust-async-std (1.12.0-2) unstable; urgency=medium

  * avoid metapackages,
    using the equivalent of debcargo setting collapse_features=true
  * unconditionally fail on testsuite error

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 28 Oct 2022 00:00:54 +0200

rust-async-std (1.12.0-1) experimental; urgency=low

  * initial Release;
    closes: Bug#1019479

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 21 Oct 2022 00:18:27 +0200
